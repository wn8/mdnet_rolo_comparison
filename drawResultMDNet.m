close all
clear
clc
warning off all;

addpath('./util');

pathAnno = '.\anno\';
pathRes = '.\results\results_MDNet\';% The folder containing the tracking results
pathDraw = '.\tmp\imgs\';% The folder that will stores the images with overlaid bounding box
pathRolo = '.\results\Rolo_Results_corrected\';

% rstIdx = 1;

seqs=configSeqsMDNet;

trks=configTrackersMDNet;

% if isempty(rstIdx)
%     rstIdx = 1;
% end

LineWidth = 4;

plotSetting;

lenTotalSeq = 0;
resultsAll=[];
trackerNames=[];
for index_seq=1:length(seqs)
    seq = seqs{index_seq};
    seq_name = seq.name;
    
    seq_length = seq.endFrame-seq.startFrame+1; %size(rect_anno,1);
    lenTotalSeq = lenTotalSeq + seq_length;
    
    for index_algrm=1:length(trks)-1
        algrm = trks{index_algrm};
        name=algrm.name;
        trackerNames{index_algrm}=name;
        
        fileName = [pathRes seq_name '_' name '.mat'];
        
        load(fileName);
        
        res = results;
        
        %         if ~isfield(res,'type')&&isfield(res,'transformType')
        %             res.type = res.transformType;
        %             res.res = res.res';
        %         end
        
        
        %         if strcmp(res.type,'rect')
        for i = 2:seq_length
            %                 r = res.res(i,:);
            r = res(i,:);
            if (isnan(r) | r(3)<=0 | r(4)<=0)
                %                     res.res(i,:)=res.res(i-1,:);
                res(i,:)=res(i-1,:);
            end
        end
        %         end
        
        resultsAll{index_algrm} = res;
        
    end
    resultsAll{index_algrm+1} = dlmread([pathRolo seq.name '\' seq.name '.txt']);
    resultsAll{index_algrm+2} = dlmread([pathAnno seq.name '.txt']);
    
    
    nz	= strcat('%0',num2str(seq.nz),'d'); %number of zeros in the name of image
    
    %     pathSave = [pathDraw seq_name '_' num2str(rstIdx) '/'];
    pathSave = [pathDraw seq_name '/'];
    if ~exist(pathSave,'dir')
        mkdir(pathSave);
    end
    
    for i=10:seq_length
        image_no = seq.startFrame + (i-1);
        id = sprintf(nz,image_no);
        fileName = strcat(seq.path,id,'.',seq.ext);
        
        img = imread(fileName);
        
        imshow(img);
        
        text(10, 15, ['#' id], 'Color','y', 'FontWeight','bold', 'FontSize',24);
        
        for j=1:length(trks)+1
            if j <= length(trks)
                disp(trks{j}.name)
            else if j == length(trks)+1
                    disp('Rolo')
                else 
                    disp('Ground Truth')
                end
            end
            
            
            LineStyle = plotDrawStyle{j}.lineStyle;
            
            rectangle('Position', resultsAll{j}(i,:), 'EdgeColor', ...
                plotDrawStyle{j}.color, 'LineWidth', LineWidth,'LineStyle',LineStyle);
            
        end
        
        imwrite(frame2im(getframe(gcf)), [pathSave  num2str(i) '.png']);
    end
    clf
end
